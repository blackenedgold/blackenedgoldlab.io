#!/bin/sh
# templated by http://qiita.com/blackenedgold/items/c9e60e089974392878c8
usage() {
    cat <<HELP
NAME:
   $0 -- resize images to width 640

SYNOPSIS:
  $0 [-h|--help]
  $0 [--verbose]

DESCRIPTION:
   resize .jpg images in current dir and siblings

  -h  --help      Print this help.
      --verbose   Enables verbose mode.

EXAMPLE:
  $ $0
  resize all .jpg files

HELP
}

main() {
    SCRIPT_DIR="$(cd $(dirname "$0"); pwd)"

    while [ $# -gt 0 ]; do
        case "$1" in
            --help) usage; exit 0;;
            --verbose) set -x; shift;;
            --) shift; break;;
            -*)
                OPTIND=1
                while getopts h OPT "$1"; do
                    case "$OPT" in
                        h) usage; exit 0;;
                    esac
                done
                shift
                ;;
            *) break;;
        esac
    done

    find . -type f -name '*.jpg' -print0 |
        xargs -0 -I@  bash -c "\
                        convert -resize 640x '@' '@.small' ; \
                        mogrify '@.small' ; \
                        mv '@.small' '@' \
            "
}

main "$@"

