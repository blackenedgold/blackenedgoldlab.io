==================================================================
https://keybase.io/blackenedgold
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://blackenedgold.gitlab.io
  * I am blackenedgold (https://keybase.io/blackenedgold) on keybase.
  * I have a public key ASCwR-77ZlzD3WaHeOUfctKGDEc0g6RPG1qa3VEpX9URQgo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120b047eefb665cc3dd668778e51f72d2860c473483a44f1b5a9add51295fd511420a",
      "host": "keybase.io",
      "kid": "0120b047eefb665cc3dd668778e51f72d2860c473483a44f1b5a9add51295fd511420a",
      "uid": "2f422931691964cabdcbcd1a4db35619",
      "username": "blackenedgold"
    },
    "merkle_root": {
      "ctime": 1528541201,
      "hash": "9f2b924c73dab2dcb706366369f17a558dbc929f9a2f73da800e7683b3534d6c037f5aca76c0549c0274c618fbad435ed17a10659b078269acb9b06b70a88652",
      "hash_meta": "e41de8f8ccc2c2a03fe48750d09e422180448feb353544bc3c2f011cdc795bd1",
      "seqno": 3050527
    },
    "service": {
      "entropy": "0BhFOAG7jhWP4shchFGpsrq4",
      "hostname": "blackenedgold.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "2.1.0"
  },
  "ctime": 1528541218,
  "expire_in": 504576000,
  "prev": "7362d500cab13cdc09460c402a67ac3e33633b4396e0edec7d6c46ce2925a34f",
  "seqno": 32,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgsEfu+2Zcw91mh3jlH3LShgxHNIOkTxtamt1RKV/VEUIKp3BheWxvYWTESpcCIMQgc2LVAMqxPNwJRgxAKmesPjNjO0OW4O3sfWxGziklo0/EIO70/ULwA2Rw9XrOR9sit1NJiKtKRmrOxFrIOF8w8Q7IAgHCo3NpZ8RAKJyQA+N0zd6U6C30lvxK9av3MUNzucc0rRNVJVqAhHlLig2E5ovOSI5EBWXfsJtLWDvKQyHxBHXazUQQdshYA6hzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIJvEqVBrQtkR/2xFwoTRssvgKp5eusxg8TD/pzn9QQ8Yo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/blackenedgold

==================================================================
