+++
showonlyimage = true
draft = false
image = "/post/無印のカッティングボートをいい感じにする/使用例.jpg"
weight = 0
categories = ["キッチン"]
date = 2020-03-01T16:37:56+09:00
title = "無印のカッティングボートをいい感じにする"
+++

最近、そのままお皿としても使えるカッティングボードが欲しくて無印で買いました。
[こういうやつ](https://www.muji.com/jp/ja/store/cmdty/detail/4549337416635)。これでおおむね満足なんですが、表面が毛羽立っていて手触りが悪いのでいい感じにしていきます。

<!--more-->


毛羽立ってるというのはこういう感じです。

![毛羽立ち](毛羽立ち.jpg)

目で見て分かるレベルで毛羽立ってますね。
見た目が悪いのもあるのですが、手触りが悪くて使ってるときに妙に不快なので加工することにします。

これにサンドペーパーをかけてワックスを塗って肌触りをよくしていきます。

サンドペーパーとワックスはAmazonで買いました。

<a href="https://www.amazon.co.jp/gp/product/B07794Z26J/ref=as_li_ss_il?ie=UTF8&psc=1&linkCode=li2&tag=diary0821-22&linkId=b5e3dd694577528ac3608e063ccb6f4d&language=ja_JP" target="_blank"><img border="0" src="//ws-fe.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B07794Z26J&Format=_SL160_&ID=AsinImage&MarketPlace=JP&ServiceVersion=20070822&WS=1&tag=diary0821-22&language=ja_JP" ></a>
<a href="https://www.amazon.co.jp/gp/product/B07HMPCRRP/ref=as_li_ss_il?ie=UTF8&psc=1&linkCode=li2&tag=diary0821-22&linkId=2303c869f5f4048eed452013a9f7c6b0&language=ja_JP" target="_blank"><img border="0" src="//ws-fe.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=B07HMPCRRP&Format=_SL160_&ID=AsinImage&MarketPlace=JP&ServiceVersion=20070822&WS=1&tag=diary0821-22&language=ja_JP" ></a>

<img src="https://ir-jp.amazon-adsystem.com/e/ir?t=diary0821-22&language=ja_JP&l=li2&o=9&a=B07794Z26J" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
<img src="https://ir-jp.amazon-adsystem.com/e/ir?t=diary0821-22&language=ja_JP&l=li2&o=9&a=B07HMPCRRP" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />

まずはサンドペーパーで表面の毛羽立ちを削ります。
最初は400番から。

![サンドペーパーの #400](サンドペーパー400.jpg)

サンドペーパーの番号は1 cm²あたりの研磨剤の数です。
番号が小さいほど研磨剤の直径が大きく粗いやすりとなり、番号が大きいほど研磨剤の直径が小さく細かいやすりとなります。
今回は仕上げてある表面の毛羽立ちを削るのが目的なので比較的粗い400番です。

サンドペーパーを平面にかけるときは硬くて平らなものに巻いて使うとかけやすいです。

![サンドペーパーと台](サンドペーパーと台.jpg)

これはネジセットのケースですが、中のネジがガチャガチャいってうるさかったのでやめました。
最終的にメガネケースに巻いて使いました。因みにやすり屑が出るのでスマホに巻いて使うのはやめといた方がいいと思います。

表面、側面、あと忘れがちですが面取りされた角にもかけていきます。
面取りの部分のように細かいところはは指でやすりをかけた方がやりやすいです。

400番をかけると表面がなめらかになります。一旦やすり屑をはらって表面を触って満足できるか確かめましょう。
ここからさらに600番をかけてもうし少し手触りをよくします。

![サンドペーパーの #600](サンドペーパー600.jpg)

これもさっきと同じようにやすりをかけます。

600番をかけたあとにさらに番号を上げていってもいいのですが、どうせまな板ですし包丁で傷つくのでこのくらいて止めておきます。

ここからワックスを塗っていきます。ワックスを塗る前に一回洗ってやすり屑を落としたんだったかな？

![ワックス前](ワックス前.jpg)

ワックスを板にとって布で塗り付けていきます。

![ワックスを乗せる](ワックスを乗せる.jpg)

ワックスは時間をかけて染み込んでいくので少し表面にベトベトが残るくらい塗るとよいです。

ワックスを塗るとこんな感じに艶が出ます。

![ワックス後](ワックス後.jpg)

このまま3日くらい置いてワックスが染み込むのを待ちます。

染み込んだら、1回だと足りないのでもう1回くらい同様に塗りました。

一手間加えるだけでぐっと良くなりますね。

![使用例](使用例.jpg)


是非試してみて下さい。
