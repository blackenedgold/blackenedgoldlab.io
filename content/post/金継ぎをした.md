+++
showonlyimage = true
draft = false
image = "/post/金継ぎをした/漆器.jpg"
weight = 0
categories = ["キッチン"]
date = 2020-02-16T23:23:17+09:00
title = "金継ぎをした"
+++

気に入っていた漆器を不注意で割ってしまったので金継ぎを試してみることにしました。
意外と難しいということが分かりました。

<!--more-->

今回継ぐのは漆器のナッツボールとニトリで買った小皿です。
ニトリの小皿は金継ぎセットが届いて金継ぎをしようとした矢先に割ってしまったのでことのついでに継いでみることにしました。

金継ぎには割れた破片をくっつける方と欠けた部分を埋める方がありますが、今回やるのはくっつける方です。
おおむね2週間かかります。

だいたいTwitterで実況したとおりなのでほぼTogetterになります。

Amazonで金継ぎセットを買って、手順書通りにやっていきます。

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">オペ（金継ぎ）を開始します <a href="https://t.co/ONmrdQ39az">pic.twitter.com/ONmrdQ39az</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1218904490197172224?ref_src=twsrc%5Etfw">January 19, 2020</a></blockquote>

片手にゴム手袋、もう片方の手は素手でやりました。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">まずは小麦粉を水で練る <a href="https://t.co/Ub9Dknr8J6">pic.twitter.com/Ub9Dknr8J6</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1218906191411048448?ref_src=twsrc%5Etfw">January 19, 2020</a></blockquote>

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">徐々に漆を加えながら練る <a href="https://t.co/I3ePFFAt56">pic.twitter.com/I3ePFFAt56</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1218907672272986123?ref_src=twsrc%5Etfw">January 19, 2020</a></blockquote>

小麦粉と水でパン生地のようなものを作ったあとに漆を加えます。
漆を加えると粘り気が出るというか、ガムっぽくなります。


このガム（麦漆）を竹串で塗っていきます。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">切断面に麦漆を塗る <a href="https://t.co/cnQCQGhSJb">pic.twitter.com/cnQCQGhSJb</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1218908372839223296?ref_src=twsrc%5Etfw">January 19, 2020</a></blockquote>

今見るとこの塗り方は甘いですね。もうちょっと薄く、均等に、全面に塗った方がいいです。


<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">パーツをくっつけていく。このまま乾燥させる。 <a href="https://t.co/YnWo7ZJoMc">pic.twitter.com/YnWo7ZJoMc</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1218911127565828111?ref_src=twsrc%5Etfw">January 19, 2020</a></blockquote>

小皿は4つのパーツに割れた上に一部欠けてしまいました。
麦漆で欠けの部分も埋められないかな、と盛ってみたのですが結論からいうとダメでしたね。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">もう一つ（本命の漆器）も継いだ。これ、ちゃんと置くの難しいわ <a href="https://t.co/JCHE1wqX6f">pic.twitter.com/JCHE1wqX6f</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1218913926009634818?ref_src=twsrc%5Etfw">January 19, 2020</a></blockquote>

漆器の方は3つに割れて、その他塗装の剥げが何箇所かありました。
なんとなく嵌め込むとピタっと元の場所に収まるイメージでしたが、全然そんなことなかったです。
麦漆でずるずる滑りますしパーツが3つになるとあっちを立てればこっちが立たず、それぞれを正しい場所に置くのは至難の技でした。
結局、ずれたままになってしまいました。

因みに、後で調べると盛り出た麦漆は拭き取るのが正解だったみたいですが、手順書にはそんなこと書いてなかったです。
あとで削ればいいやって思ってたのですが、陶器はざらざらして削りづらいですし、漆器は表面が傷付きそうで怖くて削りづらかったです。

いちおうくっついたら乾燥させます。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">ラスト室（ムロ）。湿度90%くらいの箱で1週間（化学的に）乾かす。濡らして絞った布巾を一緒に入れる。 <a href="https://t.co/rqyjOCUaHt">pic.twitter.com/rqyjOCUaHt</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1218918999427145728?ref_src=twsrc%5Etfw">January 19, 2020</a></blockquote>

湿気で乾かすというのは矛盾してるようですが、水分を抜く訳ではなくて漆液中のウルシオールをラッカーゼで酸化重合させているようです。

ウルシオールといえばかぶれる人もいるらしいですね。人によっては直接触れなくても気化したウルシオールだけでかぶれる人もいるみたいです。
私は直接手につきましたが、かぶれませんでした。


お片付けは意外と楽でした。油で拭き取れるらしいです。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">お片付け。油で拭き取ればいいらしい <a href="https://t.co/0ZhvfEffkx">pic.twitter.com/0ZhvfEffkx</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1218915562673209344?ref_src=twsrc%5Etfw">January 19, 2020</a></blockquote>

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">意外と綺麗に落ちるのね <a href="https://t.co/ELX0zKmMb0">pic.twitter.com/ELX0zKmMb0</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1218917296179044352?ref_src=twsrc%5Etfw">January 19, 2020</a></blockquote>

ここまでの作業で30分くらいです。

状況によって最短4, 5日で乾くらしいですが、週末に作業したいので1週間待って次の週末に作業します。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">1週間経ったよ。黒くかたまってる。隙間や段差があるしあんまり上手くいかなかったみたい <a href="https://t.co/pb0i7RyxEN">pic.twitter.com/pb0i7RyxEN</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1221010004980854784?ref_src=twsrc%5Etfw">January 25, 2020</a></blockquote>

先述の通りあんまり綺麗じゃなかったようです。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">ナイフとかでバリをとっていくよ <a href="https://t.co/6UT3fWNL5i">pic.twitter.com/6UT3fWNL5i</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1221010661716582406?ref_src=twsrc%5Etfw">January 25, 2020</a></blockquote>


バリを取ります。彫刻刀が刃先が小さくて使いやすかったです。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">ゴム質で割と簡単に削れる。1週間じゃ完全には乾かないのかな。 <a href="https://t.co/B0lfa2Y0xN">pic.twitter.com/B0lfa2Y0xN</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1221012895388975110?ref_src=twsrc%5Etfw">January 25, 2020</a></blockquote>

本当はヤスリとかも使って綺麗に仕上げるらしいですが、気付かずに彫刻刀で荒く削っただけで進めてしまいます。
漆器にヤスリかけたら大変なことになりそうですが大丈夫なんですかね。

因みに関係ないところについてしまった漆はこのときに油で拭き取れるらしいです。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">バリを取ったら練弁柄漆を接合部に面相筆で塗ってくよ <a href="https://t.co/7cMCEbD2Sp">pic.twitter.com/7cMCEbD2Sp</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1221015419210747906?ref_src=twsrc%5Etfw">January 25, 2020</a></blockquote>

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">こんなもん？ <a href="https://t.co/zwdcTfnsSw">pic.twitter.com/zwdcTfnsSw</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1221016688499425280?ref_src=twsrc%5Etfw">January 25, 2020</a></blockquote>

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">こっちは剥げた漆の分も塗ったけど如何に。このまま10分くらい乾かす。 <a href="https://t.co/IDg6CZO395">pic.twitter.com/IDg6CZO395</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1221017709095243779?ref_src=twsrc%5Etfw">January 25, 2020</a></blockquote>

本当はもっと繊細に線を引くものみたいです。まあ、ここは個々人の美的感覚に合わせればいいでしょう。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">多分乾いたので金粉蒔くよ。今回はアルミ製の代用金粉を使う。 <a href="https://t.co/I64rsbBniI">pic.twitter.com/I64rsbBniI</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1221020836577964032?ref_src=twsrc%5Etfw">January 25, 2020</a></blockquote>

今回買った金継ぎセットにはアルミ粉とパール粉が入ってました。
色合い的にアルミの方が元の柄に合いそうだったのでアルミ粉を使います。
本物の金を使いたい人は検索したら金継ぎ用の金粉だけ売ってます。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">蒔いた。思ったより激しいわ。 <a href="https://t.co/cOaE8DsSes">pic.twitter.com/cOaE8DsSes</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1221022399891591168?ref_src=twsrc%5Etfw">January 25, 2020</a></blockquote>

筆先に金粉を載せすぎました。蒔く前に少しはたいた方がよさそうですね。
今回はアルミ粉なので痛くないですが金粉を使うときにこれやると怖いですね。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">さらに真綿でポンポンしていく <a href="https://t.co/N3fYh0VeNQ">pic.twitter.com/N3fYh0VeNQ</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1221022926901657601?ref_src=twsrc%5Etfw">January 25, 2020</a></blockquote>

蒔いたあとは真綿でポンポンと定着させます。
筆を使わずにいきなり真綿で蒔くやり方もあるみたいです。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">できた。 <a href="https://t.co/12CycO2Az1">pic.twitter.com/12CycO2Az1</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1221023103926407168?ref_src=twsrc%5Etfw">January 25, 2020</a></blockquote>

ひとまず継ぎ目と漆が剥げてる箇所には塗れました。

また乾燥させます。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">室に戻してまた1週間 <a href="https://t.co/EZd9nNwWWj">pic.twitter.com/EZd9nNwWWj</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1221023343534428160?ref_src=twsrc%5Etfw">January 25, 2020</a></blockquote>

そしてまた次の週末を待ちます。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">１週間経ったよ <a href="https://t.co/XZnwwmFVwk">pic.twitter.com/XZnwwmFVwk</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1223545871867867136?ref_src=twsrc%5Etfw">February 1, 2020</a></blockquote>

余計な金粉を拭き取って、水洗いします。

<blockquote class="twitter-tweet" data-conversation="none"><p lang="ja" dir="ltr">くっつきはしたけど継ぎ目がきれいじゃないね。金蒔きの前にやすりかける手順があったらしいけど気づかずに進めたのが敗因かな。あと金蒔きはもっと繊細にやるものらしい <a href="https://t.co/IlY0IEmLLv">pic.twitter.com/IlY0IEmLLv</a></p>&mdash; κeen (@blackenedgold) <a href="https://twitter.com/blackenedgold/status/1223548485028306944?ref_src=twsrc%5Etfw">February 1, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

仕上がりはあまり綺麗じゃないですがひとまずくっつきました。

ちなみにお値段ですが金継ぎセット1式で1万円弱でした。
漆の保存期限は1年ほどらしいのでそんなに頻繁に皿を割る人でなければ漆は1回きりの用になるかもしれません。道具はずっと使えます。
私の場合は割った漆器が27,000円したので特に迷わず金継ぎセットを買いましたが場合によっては買い直した方が安いかもしれません。

また、私のように初手は失敗しがちだと思うので高いお皿を修理するなら一旦百均でお皿を買ってきて割って、練習した方がいいかもしれません。

まだうちに欠けた茶碗もあるので次は埋めもやってみたいですね。


