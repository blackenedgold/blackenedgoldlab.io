+++
showonlyimage = true
draft = false
image = ""
weight = 0
categories = []
date = {{ .Date }}
title = "{{ replace .TranslationBaseName "-" " " | title }}"
+++
